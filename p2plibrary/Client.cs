﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using DA.p2p;

namespace DA.p2p
{

    public class Client : IDisposable
    {
        private const int MAXRETRY = 10;
        private MyUdpClient client;
        private IPEndPoint hostIPEndPoint;
        private IPEndPoint remoteIPEndPoint;
        private UserCollection userCollection;
        private string myUserName;
        private bool ReceivedACK;
        private Thread threadListening;

        public Client(string serverIP)
        {
            ReceivedACK = false;
            remoteIPEndPoint = new IPEndPoint(IPAddress.Any, 0);
            hostIPEndPoint = new IPEndPoint(IPAddress.Parse(serverIP), Server.SERVER_LISTENING_PORT);
            client = new MyUdpClient();
            userCollection = new UserCollection();
            threadListening = new Thread(new ThreadStart(Run));
        }

        public void Start()
        {
            if (this.threadListening.ThreadState == ThreadState.Unstarted)
            {
                this.threadListening.Start();
                Console.WriteLine("You can input you command:\n");
                Console.WriteLine("Command Type:\"send\",\"exit\",\"getu\"");
                Console.WriteLine("Example : send Username Message");
                Console.WriteLine("          exit");
                Console.WriteLine("          getu");
            }
        }

        public void ConnectToServer(string userName, string password)
        {
            myUserName = userName;

            // Send login message to server
            ClientLoginMessage loginMessage = new ClientLoginMessage(userName, password);
            byte[] buffer = FormatterHelper.Serialize(loginMessage);
            client.Send(buffer, buffer.Length, hostIPEndPoint);

            // Receive response message from server
            buffer = client.Receive(ref remoteIPEndPoint);
            ServerListUserResponseMessage listUserResponseMessage 
                = (ServerListUserResponseMessage)FormatterHelper.Deserialize(buffer);

            // Update users list
            userCollection.Clear();
            foreach (User user in listUserResponseMessage.UserList)
            {
                userCollection.Add(user);
            }

            this.DisplayUsers(userCollection);
        }

        /// <summary>
        /// Use this method to send message to specific user
        /// Idea: I(Alice) will first send a message to the IP of the user (Bob).
        /// if Alice and Bob never communicate before, the message will not be delivered.
        /// Then sender will timeout. Then sender will send request server to send a
        /// message to Bob and ask Bob to send Alice a message. This way is UDP hole punching.
        /// </summary>
        /// <param name="toUserName">receiver's username</param>
        /// <param name="message">The message</param>
        /// <returns></returns>
        private bool SendMessageTo(string toUserName, string message)
        {
            User toUser = userCollection.Find(toUserName);
            if (toUser == null)
            {
                return false;
            }
            for (int i = 0; i < MAXRETRY; i++)
            {
                P2PTextMessage p2pTextMessage = new P2PTextMessage(message);
                byte[] buffer = FormatterHelper.Serialize(p2pTextMessage);
                client.Send(buffer, buffer.Length, toUser.IpEndPoint);

                // Receiving thread will mark this.ReceivedACK to true when
                // receiver ack the message
                for (int j = 0; j < 10; j++)
                {
                    if (this.ReceivedACK)
                    {
                        this.ReceivedACK = false;
                        return true;
                    }
                    else
                    {
                        Thread.Sleep(300);
                    }
                }

                // We didn't receive ACK from receiver so we need to do UDP hole punching
                ClientHolePuchingRequestMessage chprMessage = new ClientHolePuchingRequestMessage(myUserName, toUserName);
                buffer = FormatterHelper.Serialize(chprMessage);
                client.Send(buffer, buffer.Length, hostIPEndPoint);

                // We need to wait until receiver first send message to us
                Thread.Sleep(100);
            }
            return false;
        }

        public void PaserCommand(string cmdstring)
        {
            cmdstring = cmdstring.Trim();
            string[] args = cmdstring.Split(new char[] { ' ' });
            if (args.Length > 0)
            {
                if (string.Compare(args[0], "exit", true) == 0)
                {
                    ClientLogoutMessage logoutMessage = new ClientLogoutMessage(myUserName);
                    byte[] buffer = FormatterHelper.Serialize(logoutMessage);
                    client.Send(buffer, buffer.Length, hostIPEndPoint);
                    Dispose();
                    System.Environment.Exit(0);
                }
                else if (string.Compare(args[0], "send", true) == 0)
                {
                    // TODO: no heart beating test from server now...
                    if (args.Length > 2)
                    {
                        string toUserName = args[1];
                        string message = "";
                        for (int i = 2; i < args.Length; i++)
                        {
                            if (args[i] == "")
                                message += " ";
                            else
                                message += args[i];
                        }
                        if (this.SendMessageTo(toUserName, message))
                        {
                            Console.WriteLine("Send Successfull!");
                        }
                        else
                        {
                            Console.WriteLine("Send to " + toUserName + " Failed!");
                        }
                    }
                }
                else if (string.Compare(args[0], "getu", true) == 0)
                {
                    ClientListUserMessage cluMessage = new ClientListUserMessage(myUserName);
                    byte[] buffer = FormatterHelper.Serialize(cluMessage);
                    client.Send(buffer, buffer.Length, hostIPEndPoint);
                }
                else
                {
                    Console.WriteLine("Unknown command {0}", cmdstring);
                }
            }
        }

        private void DisplayUsers(UserCollection users)
        {
            foreach (User user in users)
            {
                Console.WriteLine("Username: {0}, IP:{1}, Port:{2}", user.UserName, user.IpEndPoint.Address.ToString(), user.IpEndPoint.Port);
            }
        }

        private void Run()
        {
            byte[] buffer;
            while (true)
            {
                buffer = client.Receive(ref remoteIPEndPoint); // Block receive
                object msgObj = FormatterHelper.Deserialize(buffer);
                Type messageType = msgObj.GetType();
                if (messageType == typeof(ServerListUserResponseMessage))
                {
                    // Convert message
                    ServerListUserResponseMessage slurMessage = (ServerListUserResponseMessage)msgObj;
                    // Update User list
                    userCollection.Clear();
                    foreach (User user in slurMessage.UserList)
                    {
                        userCollection.Add(user);
                    }
                    this.DisplayUsers(userCollection);
                }
                else if (messageType == typeof(ServerRequestClientDoHolePunchingMessage))
                {
                    // Convert message
                    ServerRequestClientDoHolePunchingMessage srcdhpMessage = (ServerRequestClientDoHolePunchingMessage)msgObj;
                    // Send udp punching message
                    P2PHolePunchingMessage phpMessage = new P2PHolePunchingMessage();
                    buffer = FormatterHelper.Serialize(phpMessage);
                    client.Send(buffer, buffer.Length, srcdhpMessage.RemoteIPEndPoint);
                }
                else if (messageType == typeof(P2PTextMessage))
                {
                    // Convert message
                    P2PTextMessage ptMessage = (P2PTextMessage)msgObj;
                    Console.WriteLine("Receive a message: {0}", ptMessage.Message);
                    // Send Ack
                    P2PAckMessage p2pAckMessage = new P2PAckMessage();
                    buffer = FormatterHelper.Serialize(p2pAckMessage);
                    client.Send(buffer, buffer.Length, remoteIPEndPoint);
                }
                else if (messageType == typeof(P2PAckMessage))
                {
                    this.ReceivedACK = true;
                }
                else if (messageType == typeof(P2PHolePunchingMessage))
                {
                    Console.WriteLine("Recieve a trash message");
                }
                Thread.Sleep(100);
            }
        }

        #region IDisposable Member

        public void Dispose()
        {
            try
            {
                this.threadListening.Abort();
                this.client.Close();
            }
            catch
            {
            }
        }

        #endregion
    }
}
