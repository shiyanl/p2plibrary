﻿using System;
using System.Net;
using System.Net.Sockets;


namespace DA.p2p
{
    /// <summary>
    /// Message base class
    /// </summary>
    [Serializable]
    public abstract class Message
    {
    }

    /// <summary>
    /// Client to server base class
    /// </summary>
    [Serializable]
    public abstract class Client2ServerMessage : Message
    {
        private string userName;

        public string UserName
        {
            get { return userName; }
        }

        protected Client2ServerMessage(string username)
        {
            userName = username;
        }
    }

    /// <summary>
    /// User login message
    /// </summary>
    ///  
    [Serializable]
    public class ClientLoginMessage : Client2ServerMessage
    {
        private string password;

        public string Password
        {
            get { return password; }
        }

        public ClientLoginMessage(string userName, string passWord)
            : base(userName)
        {
            this.password = passWord;
        }
    }

    /// <summary>
    /// User logout message
    /// </summary>
    [Serializable]
    public class ClientLogoutMessage : Client2ServerMessage
    {
        public ClientLogoutMessage(string userName)
            : base(userName)
        {
        }
    }

    /// <summary>
    /// Request user list message
    /// </summary>
    [Serializable]
    public class ClientListUserMessage : Client2ServerMessage
    {
        public ClientListUserMessage(string userName)
            : base(userName)
        {
        }
    }

    /// <summary>
    /// Request server to assist receiver to make udp hole punching message.
    /// </summary>
    [Serializable]
    public class ClientHolePuchingRequestMessage : Client2ServerMessage
    {
        protected string toUserName;

        public ClientHolePuchingRequestMessage(string userName, string toUserName)
            : base(userName)
        {
            this.toUserName = toUserName;
        }

        public string ToUserName
        {
            get { return this.toUserName; }
        }
    }

    /// <summary>
    /// Server to client message base
    /// </summary>
    [Serializable]
    public abstract class Server2ClientMessage : Message
    {
    }

    /// <summary>
    /// Response user list update request
    /// </summary>
    [Serializable]
    public class ServerListUserResponseMessage : Server2ClientMessage
    {
        private UserCollection userList;

        public ServerListUserResponseMessage(UserCollection users)
        {
            this.userList = users;
        }

        public UserCollection UserList
        {
            get { return userList; }
        }
    }

    /// <summary>
    /// forward udp hole punching request message (Server help forward to receiver)
    /// </summary>
    [Serializable]
    public class ServerRequestClientDoHolePunchingMessage : Server2ClientMessage
    {
        protected System.Net.IPEndPoint remoteIPEndPoint;

        public ServerRequestClientDoHolePunchingMessage(System.Net.IPEndPoint point)
        {
            this.remoteIPEndPoint = point;
        }

        public System.Net.IPEndPoint RemoteIPEndPoint
        {
            get { return remoteIPEndPoint; }
        }
    }


    /// <summary>
    /// p2p message base
    /// </summary>
    [Serializable]
    public abstract class P2PMessage : Message
    {
    }

    /// <summary>
    /// p2p text message
    /// </summary>
    [Serializable]
    public class P2PTextMessage : P2PMessage
    {
        private string message;

        public P2PTextMessage(string msg)
        {
            message = msg;
        }

        public string Message
        {
            get { return message; }
        }
    }

    /// <summary>
    /// p2p text message ack
    /// </summary>
    [Serializable]
    public class P2PAckMessage : P2PMessage
    {
    }

    /// <summary>
    /// udp hole punching message
    /// </summary>
    [Serializable]
    public class P2PHolePunchingMessage : P2PMessage
    {
    }
}
