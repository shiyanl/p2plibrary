﻿using System;
using System.Net;
using System.Net.Sockets;

namespace DA.p2p
{
    class MyUdpClient : UdpClient // System.Net.Sockets
    {
        public MyUdpClient()
            : base()
        {
        }

        public MyUdpClient(int port)
            : base(port)
        {            
        }

        // Just add a debugging layer on the top of base method
        new public int Send(byte[] data, int bytes, IPEndPoint remoteHost)
        {
            int ret = base.Send(data, bytes, remoteHost);
            System.Console.WriteLine(System.DateTime.Now.ToString() + ">>> " + remoteHost);
            System.Console.WriteLine(FormatterHelper.Deserialize(data));
            System.Console.WriteLine();
            return ret;
        }

        // Just add a debugging layer on the top of base method
        new public byte[] Receive(ref IPEndPoint remoteHost)
        {
            byte[] data = base.Receive(ref remoteHost);
            System.Console.WriteLine(System.DateTime.Now.ToString() + "<<< " + remoteHost);
            System.Console.WriteLine(FormatterHelper.Deserialize(data));
            System.Console.WriteLine();
            return data;
        }
    }
}
