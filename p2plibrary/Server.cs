﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using DA.p2p;

namespace DA.p2p
{
    
    public class Server
    {
        public const int SERVER_LISTENING_PORT = 2280;

        private MyUdpClient udpServer;
        private UserCollection userCollection;
        private Thread threadServer;
        private IPEndPoint remoteEndPoint;

        // Constructor
        public Server()
        {
            userCollection = new UserCollection();
            remoteEndPoint = new IPEndPoint(IPAddress.Any, 0);
            threadServer = new Thread(new ThreadStart(Run)); // Put Run method in the new thread
        }

        // Start the Udp Server
        public void Start()
        {
            try
            {
                udpServer = new MyUdpClient(SERVER_LISTENING_PORT);
                threadServer.Start();
                Console.WriteLine("P2P Central Server started, waiting clients to connect...");
            }
            catch (Exception exp)
            {
                Console.WriteLine("Failed to start P2P Server: " + exp.Message);
                throw exp;
            }
        }

        // Stop the Udp Server
        public void Stop()
        {
            Console.WriteLine("Stopping P2P Server...");
            try
            {
                threadServer.Abort();
                udpServer.Close();
                Console.WriteLine("P2P Central Server stopped.");
            }
            catch (Exception exp)
            {
                Console.WriteLine("Failed to stop P2P Server: " + exp.Message);
                throw exp;
            }

        }

        private void Run()
        {
            byte[] buffer = null;
            while (true)
            {
                // The Receive method will block until a datagram arrives from a remote host.
                byte[] messageBuffer = udpServer.Receive(ref remoteEndPoint);
                try
                {
                    // Deserialize the message
                    object msgObj = FormatterHelper.Deserialize(messageBuffer);
                    Type messageType = msgObj.GetType(); // Get the type of the message
                    if (messageType == typeof(ClientLoginMessage))
                    {
                        // Convert message
                        ClientLoginMessage loginMessage = (ClientLoginMessage)msgObj;
                        Console.WriteLine("{0}: user {1} sign in.", System.DateTime.Now.ToString(), loginMessage.UserName);

                        // Add user to the list
                        IPEndPoint userEndPoint = new IPEndPoint(remoteEndPoint.Address, remoteEndPoint.Port);
                        User user = new User(loginMessage.UserName, userEndPoint);
                        userCollection.Add(user);

                        // Send response message
                        ServerListUserResponseMessage usersMsg = new ServerListUserResponseMessage(userCollection);
                        buffer = FormatterHelper.Serialize(usersMsg);
                        udpServer.Send(buffer, buffer.Length, remoteEndPoint);
                        Console.WriteLine("Send:" + usersMsg);
                    }
                    else if (messageType == typeof(ClientLogoutMessage))
                    {
                        // Convert message
                        ClientLogoutMessage logoutMessage = (ClientLogoutMessage)msgObj;
                        Console.WriteLine("{0}: {1} sign out", System.DateTime.Now.ToString(), logoutMessage.UserName);

                        // remove user from the list
                        User logoutUser = userCollection.Find(logoutMessage.UserName);
                        if (logoutUser != null)
                        {
                            userCollection.Remove(logoutUser);
                        }
                    }
                    else if (messageType == typeof(ClientHolePuchingRequestMessage))
                    {
                        // Convert message
                        ClientHolePuchingRequestMessage holePunchingRequestMessage = (ClientHolePuchingRequestMessage)msgObj;
                        Console.WriteLine("{0}(1) wants to p2p {2}", remoteEndPoint.Address.ToString(), holePunchingRequestMessage.UserName, holePunchingRequestMessage.ToUserName);

                        // Get receiver
                        User toUser = userCollection.Find(holePunchingRequestMessage.ToUserName);

                        // Forward udp hole punching message to the receiver
                        if (toUser == null)
                        {
                            Console.WriteLine("Remote host {0} cannot be found at index server", holePunchingRequestMessage.ToUserName);
                        }
                        else
                        {
                            // TODO: Udp hole punching hasn't been fully tested
                            ServerRequestClientDoHolePunchingMessage pingMessage = new ServerRequestClientDoHolePunchingMessage(remoteEndPoint);
                            buffer = FormatterHelper.Serialize(pingMessage);
                            udpServer.Send(buffer, buffer.Length, toUser.IpEndPoint);
                            Console.WriteLine("Send:" + pingMessage);
                        }
                    }
                    else if (messageType == typeof(ClientListUserMessage))
                    {
                        // Send to all logged in users
                        ServerListUserResponseMessage slurMessage = new ServerListUserResponseMessage(userCollection);
                        buffer = FormatterHelper.Serialize(slurMessage);
                        foreach (User user in userCollection)
                        {
                            udpServer.Send(buffer, buffer.Length, user.IpEndPoint);
                            Console.WriteLine("Send:" + slurMessage);
                        }
                    }
                    Thread.Sleep(500);
                }
                catch (Exception e)
                {
                    System.Console.WriteLine(e.Message);
                    System.Console.WriteLine(e.StackTrace);
                }
            }
        }
    }
}


