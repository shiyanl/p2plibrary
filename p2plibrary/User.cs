﻿using System;
using System.Net;
using System.Collections;
using System.Collections.Generic;

namespace DA.p2p
{
    /// <summary>
    /// User class
    /// </summary>
    [Serializable]
    public class User
    {
        protected string userName;
        protected IPEndPoint ipEndPoint;

        public User(string UserName, IPEndPoint ipEndPoint)
        {
            this.userName = UserName;
            this.ipEndPoint = ipEndPoint;
        }

        public string UserName
        {
            get { return userName; }
        }

        public IPEndPoint IpEndPoint
        {
            get { return ipEndPoint; }
            set { ipEndPoint = value; }
        }
    }

    /// <summary>
    /// User collection
    /// </summary>
    [Serializable]
    public class UserCollection : CollectionBase // System.Collections
    {
        public void Add(User user)
        {
            InnerList.Add(user);
        }

        public void Remove(User user)
        {
            InnerList.Remove(user);
        }

        // Override []operator
        public User this [int index]
        {
            get { return (User)InnerList[index]; }
        }

        public User Find(string userName)
        {
            foreach (User user in this)
            {
                if (string.Compare(userName, user.UserName, true) == 0)// Ignore case
                {
                    return user;
                }
            }
            return null;
        }
    }
}
