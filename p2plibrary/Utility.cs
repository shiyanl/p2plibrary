﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace DA.p2p
{
    /// <summary>
    /// FormatterHelper serilization and deserilization
    /// </summary>
    public class FormatterHelper
    {
        public static byte[] Serialize(object obj)
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            MemoryStream memoryStream = new MemoryStream(1024 * 10);
            binaryFormatter.Serialize(memoryStream, obj);
            memoryStream.Seek(0, SeekOrigin.Begin);
            byte[] buffer = new byte[(int)memoryStream.Length];
            memoryStream.Read(buffer, 0, buffer.Length);
            memoryStream.Close();
            return buffer;
        }

        public static object Deserialize(byte[] buffer)
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            MemoryStream memoryStream = new MemoryStream(buffer, 0, buffer.Length, false);
            object obj = binaryFormatter.Deserialize(memoryStream);
            memoryStream.Close();
            return obj;
        }
    }
}
