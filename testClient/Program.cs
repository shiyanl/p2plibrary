﻿using System;
using DA.p2p;

namespace testClient
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Client client = new Client("127.0.0.1");
            client.ConnectToServer(args[0], "welcome");
            client.Start();
            Console.WriteLine("test arguments");
            while (true)
            {
                string str = Console.ReadLine();
                client.PaserCommand(str);
            }
        }
    }
}
