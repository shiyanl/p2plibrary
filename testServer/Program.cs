﻿using System;
using DA.p2p;

namespace testServer
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Server server = new Server();
            try
            {
                server.Start();
                Console.ReadLine();
                Console.ReadLine();
                server.Stop();
            }
            catch
            {
            }
        }
    }
}
